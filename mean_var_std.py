import numpy as np

def calculate(list):

    if len(list) != 9:
        raise ValueError('List must contain nine numbers.')
    L = np.array(list).reshape(3,3)
    calculations = {
        'mean': [np.mean(L, axis=0).tolist(), np.mean(L, axis=1).tolist(), np.mean(L)],
        'variance': [np.var(L, axis=0).tolist(), np.var(L, axis=1).tolist(), np.var(L)],
        'standard deviation': [np.std(L, axis=0).tolist(), np.std(L, axis=1).tolist(), np.std(L)],
        'max': [np.max(L, axis=0).tolist(), np.max(L, axis=1).tolist(), np.max(L)],
        'min': [np.min(L, axis=0).tolist(), np.min(L, axis=1).tolist(), np.min(L)],
        'sum': [np.sum(L, axis=0).tolist(), np.sum(L, axis=1).tolist(), np.sum(L)]
    }

    return calculations